#!/home/jhbravo/Software/miniconda3/envs/pytroll/bin/python
#/home/smalo/anaconda3/bin/python

from http.server import HTTPServer, CGIHTTPRequestHandler
import os

if __name__ == '__main__':
    try:
    
        web_dir = os.path.dirname(__file__)
        os.chdir(f"{web_dir}")
        
        CGIHTTPRequestHandler.cgi_directories = ['/cgi-bin']

        httpd = HTTPServer(('', 8000),             # localhost:8000
                           CGIHTTPRequestHandler)  # CGI support.

        print(f"Running server. Use [ctrl]-c to terminate.")

        httpd.serve_forever()

    except KeyboardInterrupt:
        print(f"\nReceived keyboard interrupt. Shutting down server.")
        httpd.socket.close()
        
        
 #/home/smalo/anaconda3/bin/python
 #
