var boundsMEX = new L.LatLngBounds(
  new L.LatLng(13.51886102885226393, -148.84630656290616457),
  new L.LatLng(57.32233157577876881, -50.74642694976628832)
);
var bounds = new L.LatLngBounds(
  new L.LatLng(15.2, -121.8),
  new L.LatLng(34.2, -86.5)
);

// Raster Layer background, source: https://leaflet-extras.github.io/leaflet-providers/preview/
var GeoportailFrance_orthos = L.tileLayer(
  "https://wxs.ign.fr/{apikey}/geoportail/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE={style}&TILEMATRIXSET=PM&FORMAT={format}&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}",
  {
    attribution:
      '<a target="_blank" href="https://www.geoportail.gouv.fr/">Geoportail France</a>',
    bounds: [
      [-75, -180],
      [81, 180],
    ],
    minZoom: 2,
    maxZoom: 19,
    apikey: "choisirgeoportail",
    format: "image/jpeg",
    style: "normal",
  }
);
var Wikimedia = L.tileLayer(
  "https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}{r}.png",
  {
    attribution:
      '<a href="https://wikimediafoundation.org/wiki/Maps_Terms_of_Use">Wikimedia</a>',
    minZoom: 1,
    maxZoom: 19,
  }
);
var NASAGIBS_ViirsEarthAtNight2012 = L.tileLayer(
  "https://map1.vis.earthdata.nasa.gov/wmts-webmerc/VIIRS_CityLights_2012/default/{time}/{tilematrixset}{maxZoom}/{z}/{y}/{x}.{format}",
  {
    attribution:
      'Imagery provided by services from the Global Imagery Browse Services (GIBS), operated by the NASA/GSFC/Earth Science Data and Information System (<a href="https://earthdata.nasa.gov">ESDIS</a>) with funding provided by NASA/HQ.',
    bounds: [
      [-85.0511287776, -179.999999975],
      [85.0511287776, 179.999999975],
    ],
    minZoom: 1,
    maxZoom: 8,
    format: "jpg",
    time: "",
    tilematrixset: "GoogleMapsCompatible_Level",
  }
);
// Raster layer labels, source: https://leaflet-extras.github.io/leaflet-providers/preview/
var LightOnlyLabels = L.tileLayer(
  "https://{s}.basemaps.cartocdn.com/light_only_labels/{z}/{x}/{y}{r}.png",
  {
    attribution:
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
    subdomains: "abcd",
    maxZoom: 19,
  }
);
var DarkOnlyLabels = L.tileLayer(
  "https://{s}.basemaps.cartocdn.com/dark_only_labels/{z}/{x}/{y}{r}.png",
  {
    attribution:
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
    subdomains: "abcd",
    maxZoom: 19,
  }
);
var VoyagerOnlyLabels = L.tileLayer(
  "https://{s}.basemaps.cartocdn.com/rastertiles/voyager_only_labels/{z}/{x}/{y}{r}.png",
  {
    attribution:
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
    subdomains: "abcd",
    maxZoom: 19,
  }
);
var Hydda_RoadsAndLabels = L.tileLayer(
  "https://{s}.tile.openstreetmap.se/hydda/roads_and_labels/{z}/{x}/{y}.png",
  {
    maxZoom: 18,
    attribution:
      'Tiles courtesy of <a href="http://openstreetmap.se/" target="_blank">OpenStreetMap Sweden</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
  }
);
var Stamen_TonerLabels = L.tileLayer(
  "https://stamen-tiles-{s}.a.ssl.fastly.net/toner-labels/{z}/{x}/{y}{r}.{ext}",
  {
    attribution:
      'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    subdomains: "abcd",
    minZoom: 0,
    maxZoom: 20,
    ext: "png",
  }
);

//  .. White background
var white = L.tileLayer(
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEAAQMAAABmvDolAAAAA1BMVEX///+nxBvIAAAAH0lEQVQYGe3BAQ0AAADCIPunfg43YAAAAAAAAAAA5wIhAAAB9aK9BAAAAABJRU5ErkJggg=="
);
// Point Layers
var MexCit = new L.GeoJSON.AJAX("data/ciudades.geojson", {
  onEachFeature: function (feature, featureLayer) {
    //featureLayer.bindPopup(feature.properties.NAME ),//+ ' | ' + feature.properties.Municipio + '<br>' + feature.geometry.coordinates);
    featureLayer.bindTooltip(feature.properties.NOMBRE, {
      closeButton: false,
      offset: L.point(0, -5),
    }); //+ ' | ' + feature.properties.Municipio + '<br>' + feature.geometry.coordinates);
  },
  pointToLayer: function (feature, latlng) {
    return L.marker(latlng, {
      icon: L.icon({
        iconUrl: feature.properties.icon_exp,
        iconSize: [32, 37],
        iconAnchor: [16, 37],
        popupAnchor: [0, -28],
      }),
    });
  },
});
// Municipios de Veracruz
var LocsVer = new L.GeoJSON.AJAX("data/ver_locs.geojson", {
  onEachFeature: function (feature, featureLayer) {
    //featureLayer.bindPopup(feature.properties.NAME ),//+ ' | ' + feature.properties.Municipio + '<br>' + feature.geometry.coordinates);
    featureLayer.bindTooltip(feature.properties.Nombre, {
      closeButton: false,
      offset: L.point(0, -5),
    }); //+ ' | ' + feature.properties.Municipio + '<br>' + feature.geometry.coordinates);
  },
  pointToLayer: function (feature, latlng) {
    return L.marker(latlng, {
      icon: L.icon({
        iconUrl: feature.properties.icon_exp,
        iconSize: [32, 37],
        iconAnchor: [16, 37],
        popupAnchor: [0, -28],
      }),
    });
  },
});

// Vector Layers
var MexEdos = new L.GeoJSON.AJAX("data/MexEdos.geojson", {
  crs: L.CRS.EPSG4326,
  style: {
    color: "#00c2ff",
    weight: 1.2,
    opacity: 0.95,
    fillColor: "#00c2ff",
    fillOpacity: 0,
  },
  onEachFeature: function (feature, featureLayer) {
    featureLayer.bindPopup(
      feature.properties.CODE + " | " + feature.properties.NAME
    );
  },
});
// Vector Layers
var MpiosVer = new L.GeoJSON.AJAX("data/ver_mpios.geojson", {
  crs: L.CRS.EPSG4326,
  style: {
    color: "#3048c6",
    weight: 1.2,
    opacity: 0.95,
    fillColor: "#3048c6",
    fillOpacity: 0,
  },
  onEachFeature: function (feature, featureLayer) {
    featureLayer.bindPopup(
      feature.properties.ID_MUN + " | " + feature.properties.NOM_MUN
    );
  },
});
var GoMeMarC = new L.GeoJSON.AJAX("data/GoMeMarC.geojson", {
  crs: L.CRS.EPSG4326,
  style: {
    color: "#ff7800",
    weight: 0.5,
    opacity: 0.95,
    fillColor: "#ff7800",
    fillOpacity: 0,
  },
  onEachFeature: function (feature, featureLayer) {
    featureLayer.bindPopup(
      feature.properties.ID + " | " + feature.properties.Name
    );
  },
});
//g16_GoMeMarC.addTo(map);
var SEMAR = new L.GeoJSON.AJAX("data/SERMAR.geojson", {
  crs: L.CRS.EPSG4326,
  style: {
    color: "#ff7800",
    weight: 0.5,
    opacity: 0.95,
    fillColor: "#ff7800",
    fillOpacity: 0,
  },
  onEachFeature: function (feature, featureLayer) {
    featureLayer.bindPopup(
      feature.properties.NOMBRE + " | " + feature.properties.nombre_2
    );
  },
});

var SurCentAmer = new L.GeoJSON.AJAX("data/SurCentAmer.geojson", {
  crs: L.CRS.EPSG4326,
  style: {
    color: "#ff7800",
    weight: 0.5,
    opacity: 0.95,
    fillColor: "#ff7800",
    fillOpacity: 0,
  },
  onEachFeature: function (feature, featureLayer) {
    featureLayer.bindPopup(
      feature.properties.FIPS_CNTRY + " | " + feature.properties.CNTRY_NAME
    );
  },
});

var Guate = new L.GeoJSON.AJAX("data/Guatemala.geojson", {
  crs: L.CRS.EPSG4326,
  style: {
    color: "#ff7800",
    weight: 0.5,
    opacity: 0.95,
    fillColor: "#ff7800",
    fillOpacity: 0,
  },
  onEachFeature: function (feature, featureLayer) {
    featureLayer.bindPopup(
      feature.properties.FIPS_CNTRY + " | " + feature.properties.CNTRY_NAME
    );
  },
});


var US_states = new L.GeoJSON.AJAX("data/us_states.geojson", {
  crs: L.CRS.EPSG4326,
  style: {
    color: "#ff7800",
    weight: 0.5,
    opacity: 0.95,
    fillColor: "#ff7800",
    fillOpacity: 0,
  },
  onEachFeature: function (feature, featureLayer) {
    featureLayer.bindPopup(
      feature.properties.STATEFP + " | " + feature.properties.NAME
    );
  },
});

// Get url parameters
// source: https://blog.mastermaps.com/2012/10/how-to-control-your-leaflet-map-with.html
var params = {};
window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (
  m,
  key,
  value
) {
  params[key] = value;
});

if (params.layers) {
  var activeLayers = params.layers.split(",").map(function (item) {
    // map function not supported in IE < 9
    return layers[item];
  });
}

// Create map
var map = L.map("map", {
  center: [params.lat || 15.5, params.lng || -90],
  zoom: params.zoom || 6,
  minZoom: 5,
  maxZoom: 15,
  layers: [SurCentAmer], //[LocsVer, MexEdos, MpiosVer],
  //layers: [MexEdos], //[LocsVer, MexEdos, MpiosVer],
  zoomControl: true,
});
white.addTo(map);
// moving Zoom Control, I found two options, and I use the second
//https://gis.stackexchange.com/questions/183813/tweaking-location-of-zoomcontrol-cartodb-js
//https://stackoverflow.com/questions/22926512/customize-zoom-in-out-button-in-leaflet-js

// source1: https://gis.stackexchange.com/questions/310410/keep-the-zoom-level-and-the-center-of-the-screen-view-when-reloading-a-web-page
// Retrieve
var mapZoomLevel = localStorage.theZoom;
var mapCenterPos = localStorage.theCenter;

if (mapZoomLevel == undefined || mapZoomLevel == null) {
  mapZoomLevel = 6; //default
  //document.getElementById("demo1").innerHTML = mapZoomLevel;
}

if (mapCenterPos == undefined || mapCenterPos == null) {
  mapCenterPos = [24.4, -96]; //default
  //document.getElementById("demo1").innerHTML = mapCenterPos;
}

map.on("load", function (e) {
  map.panTo(mapCenterPos);
  map.setZoom(mapZoomLevel);
});
//store
map.on("moveend", function (e) {
  localStorage.theCenter = map.getCenter();
  localStorage.theZoom = map.getZoom();
});
// end source1

var rstrmaps = {};
var vecpts = {
  //Ciudades: MexCit,
  // "Ciudades Ver": LocsVer,
  "<span style='color: rgb(138, 240, 153),'>Centroamérica, Suramérica y Caribe</span>": SurCentAmer,
  "Departamentos de Guatemala": Guate,
  //"US States": US_states,
  //"GoM & MarC": GoMeMarC,
  //"Zonas SEMAR": SEMAR,
  // "Capitales": g16_capmex,
  //"Sur & Cent Am": SurCentAmer,
};

// Add base layers
ctrllyr = L.control.layers(rstrmaps, vecpts, {
  collapsed: false,
});
ctrllyr.addTo(map);

// // Para agregar imagenes
// L.Control.Watermark = L.Control.extend({
//   onAdd: function (map) {
//     var div = L.DomUtil.create("div", "logo");

//     div.innerHTML =
//       '    <a href="http://www.veracruz.gob.mx/proteccioncivil/" target="_blank"><img class="img-responsive" src="imgs/SPCVer1.png" alt="SPC-Ver"></a>\
//             <a href="http://www.veracruz.gob.mx/proteccioncivil/" target="_blank"><img class="img-responsive" src="imgs/SPCVer2.png" alt="SPC-Ver"></a>\
//             <a href="http://www.veracruz.gob.mx/proteccioncivil/" target="_blank"><img class="img-responsive" src="imgs/SPCVer3.png" alt="SPC-Ver"></a>';
//     return div;
//   },
//   onRemove: function (map) {
//     // Nothing to do here
//   },
// });

// L.control.watermark = function (opts) {
//   return new L.Control.Watermark(opts);
// };
// L.control.watermark({ position: "bottomleft" }).addTo(map);

// Para agregar imagenes: powered by
L.Control.Watermark = L.Control.extend({
  onAdd: function (map) {
    var div = L.DomUtil.create("div", "powered");

    div.innerHTML =
      '<a href="https://www.python.org/"   target="_blank"><img class="img-responsive" src="imgs/power_python_128.png"  alt="python" ></a>\
       <a href="https://earthengine.google.com/" target="_blank"><img class="img-responsive" src="imgs/power_gee.png" alt="pytroll"></a>\
       <a href="https://leafletjs.com/"    target="_blank"><img class="img-responsive" src="imgs/power_leaflet_128.png" alt="leaflet"></a>';
    return div;
  },
  onRemove: function (map) {
    // Nothing to do here
  },
});

L.control.watermark = function (opts) {
  return new L.Control.Watermark(opts);
};
L.control.watermark({ position: "bottomright" }).addTo(map);
// Para agregar imagenes: powered by

// Zoom (moverlo)
map.zoomControl.setPosition("bottomright");
// Zoom end

// Escala
L.control
  .scale({
    imperial: false,
  })
  .addTo(map);
// Escala end

// Para agregar la hora
L.Control.Watermark = L.Control.extend({
  onAdd: function (map) {
    var div = L.DomUtil.create("div", "info");

    div.innerHTML = '<h3 id="utctime" "></h3>\
      <h3 id="loctime" "></h3>';
    return div;
  },
  onRemove: function (map) {
    // Nothing to do here
  },
});

L.control.watermark = function (opts) {
  return new L.Control.Watermark(opts);
};
L.control.watermark({ position: "topleft" }).addTo(map);
// Para agregar la hora: END
