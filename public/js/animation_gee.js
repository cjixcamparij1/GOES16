/* Define the Animation class */
function Animation(frames, img_id, slider_id, loop_select_id) {
  this.img_id = img_id;
  this.slider_id = slider_id;
  this.loop_select_id = loop_select_id;
  this.interval = 750;
  this.current_frame = 0;
  this.direction = 0;
  this.timer = null;
  this.frames = new Array(frames.length);

  for (var i = 0; i < frames.length; i++) {
    this.frames[i] = new Image();
    this.frames[i].src = frames[i];
  }
  document.getElementById(this.slider_id).max = this.frames.length - 1;
  this.set_frame(this.current_frame);
}

Animation.prototype.get_loop_state = function () {
  var button_group = document[this.loop_select_id].state;
  for (var i = 0; i < button_group.length; i++) {
    var button = button_group[i];
    if (button.checked) {
      return button.value;
    }
  }
  return undefined;
};

Animation.prototype.set_frame = function (frame) {
  this.current_frame = frame;

  preimageUrl = this.frames[this.current_frame].src;
  var lsdata = preimageUrl.split("_");
  var endv = lsdata.length;

  var preurlgee = 'https://earthengine.googleapis.com/v1alpha/projects/earthengine-legacy/maps/????/tiles/{z}/{x}/{y}'
  // var idgee = 'eae1a9fe5d2d779f7e5298058b2ec2d4-ae915eb51d73c216f63046829e7eaf9a'
  var idgee = lsdata[endv - 2];

  urlgee = preurlgee.replace("????", idgee);

  var baseMap = new L.tileLayer(urlgee, {
    "attribution": "Google Earth Engine",
    "detectRetina": false,
    "maxNativeZoom": 18,
    "maxZoom": 18,
    "minZoom": 0,
    "noWrap": false,
    "opacity": 1,
    "subdomains": "abc",
    "tms": false
  });

  //document.getElementById("demo").innerHTML = this.current_frame;
  //document.getElementById("txtURL").innerHTML = imageUrl;
  var tempvar = document.getElementsByClassName("leaflet-layer").length;
  //document.getElementById("demo1").innerHTML = tempvar

  map.addLayer(baseMap);

  var datestr = lsdata[endv - 1];
  var d1 = new Date(datestr + 'Z');
  //var d1 = new Date(parseInt(yy),parseInt(mm)-1,parseInt(dd),parseInt(hh),parseInt(mn),parseInt(ss),);

  var d1str = datestr.replace("T", " ");
  document.getElementById("utctime").innerHTML = d1str + " UTC";
  var d2 = new Date(d1);

  var d2str = d2.getFullYear() + "-" + ("0" + (d2.getMonth() + 1)).slice(-2) + "-" + ("0" + d2.getDate()).slice(-2) + " " +
   ("0" + d2.getHours()).slice(-2) + ":" + ("0" + d2.getMinutes()).slice(-2) + ":" + ("0" + d2.getSeconds()).slice(-2);
  document.getElementById("loctime").innerHTML = d2str + " LOC";

  //goes16lyr.getContainer().className += "todelet"+this.current_frame

  //document.getElementById("demo1").innerHTML = document.getElementsByClassName("leaflet-layer todelet0").length

  if (this.current_frame > 0) {
    var ind = this.current_frame - 1;
    //document.getElementById("demo1").innerHTML = this.frames.length
    //image_x = document.getElementsByClassName("leaflet-layer todelet"+ind.toString())[0];
    image_x = document.getElementsByClassName("leaflet-layer")[0];
    image_x.parentNode.removeChild(image_x);
  }
  if (this.current_frame == this.frames.length - 1) {
    var ind = this.current_frame;
    //image_x = document.getElementsByClassName("leaflet-layer todelet"+ind.toString())[0];
    image_x = document.getElementsByClassName("leaflet-layer")[0];
    image_x.parentNode.removeChild(image_x);
  }

  document.getElementById(this.slider_id).value = this.current_frame;
};

Animation.prototype.next_frame = function () {
  this.set_frame(Math.min(this.frames.length - 1, this.current_frame + 1));
};

Animation.prototype.previous_frame = function () {
  this.set_frame(Math.max(0, this.current_frame - 1));
};

Animation.prototype.first_frame = function () {
  this.set_frame(0);
};

Animation.prototype.last_frame = function () {
  this.set_frame(this.frames.length - 1);
};

Animation.prototype.slower = function () {
  this.interval /= 0.7;
  if (this.direction > 0) {
    this.play_animation();
  } else if (this.direction < 0) {
    this.reverse_animation();
  }
};

Animation.prototype.faster = function () {
  this.interval *= 0.7;
  if (this.direction > 0) {
    this.play_animation();
  } else if (this.direction < 0) {
    this.reverse_animation();
  }
};

Animation.prototype.anim_step_forward = function () {
  this.current_frame += 1;
  if (this.current_frame < this.frames.length) {
    this.set_frame(this.current_frame);
  } else {
    var loop_state = this.get_loop_state();
    if (loop_state == "loop") {
      this.first_frame();
    } else if (loop_state == "reflect") {
      this.last_frame();
      this.reverse_animation();
    } else {
      this.pause_animation();
      this.last_frame();
    }
  }
};

Animation.prototype.anim_step_reverse = function () {
  this.current_frame -= 1;
  if (this.current_frame >= 0) {
    this.set_frame(this.current_frame);
  } else {
    var loop_state = this.get_loop_state();
    if (loop_state == "loop") {
      this.last_frame();
    } else if (loop_state == "reflect") {
      this.first_frame();
      this.play_animation();
    } else {
      this.pause_animation();
      this.first_frame();
    }
  }
};

Animation.prototype.pause_animation = function () {
  this.direction = 0;
  if (this.timer) {
    clearInterval(this.timer);
    this.timer = null;
  }
};

Animation.prototype.play_animation = function () {
  this.pause_animation();
  this.direction = 1;
  var t = this;
  if (!this.timer)
    this.timer = setInterval(function () {
      t.anim_step_forward();
    }, this.interval);
};

Animation.prototype.reverse_animation = function () {
  this.pause_animation();
  this.direction = -1;
  var t = this;
  if (!this.timer)
    this.timer = setInterval(function () {
      t.anim_step_reverse();
    }, this.interval);
};
